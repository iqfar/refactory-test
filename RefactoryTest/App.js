import React, {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native';

const App = () => {
  const [input, setInput] = useState('');
  const [count, setCount] = useState(0);
  const [mementos, setMementos] = useState([]);
  const [reverse, setReverse] = useState(false);

  const saveMemento = () => {
    const arr = input.split('');
    if (mementos.length > 0) {
      mementos.push(arr[arr.length - 1]);
    } else {
      arr.forEach((item) => {
        mementos.push(item);
      });
    }
  };

  console.log(mementos);

  const undo = () => {
    const lastMemento = mementos.pop();

    input === lastMemento ? lastMemento : input;
  };

  const onChangeText = (val) => {
    setInput(val);
  };

  const onPress = () => {
    console.log(count);
    console.log(mementos);
    setCount(count + 1);
    if (count % 2 === 1) {
      undo();
    } else {
      saveMemento();
    }
  };

  const onReverse = () => {
    setMementos(mementos.reverse());
    setReverse(true);
  };

  let text;
  text = mementos.join('');

  return (
    <View style={styles.container}>
      <Text>App</Text>
      <TextInput
        onChangeText={onChangeText}
        style={styles.input}
        value={input}
      />
      <View style={styles.button}>
        <Button title="Reverse" onPress={onReverse} />
        <Button title="Undo/Redo" onPress={onPress} />
      </View>
      <Text style={{textAlign: 'center'}}>output : {text}</Text>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    borderWidth: 1,
    margin: 20,
    paddingHorizontal: 14,
  },
  button: {
    flexDirection: 'row',
    alignSelf: 'center',
    margin: 20,
    justifyContent: 'space-evenly',
  },
});
